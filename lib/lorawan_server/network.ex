defmodule LorawanBridge.LorawanServer.Network do
  alias LorawanBridge.LorawanServer

  @moduledoc """
  {"name":"EIOTA","netid":"000000","region":"US902","tx_codr":"4/5","join1_delay":5,"join2_delay":6,"rx1_delay":1,"rx2_delay":2,"gw_power":26,"max_eirp":30,"max_power":0,"min_power":10,"max_datr":4,"dcycle_init":0,"rxwin_init":{"rx1_dr_offset":0,"rx2_dr":8,"rx2_freq":923.3},"init_chans":"0-71","cflist":null}
  """
  defstruct [
    :name,
    :netid,
    :region,
    :tx_codr,
    :join1_delay,
    :join2_delay,
    :rx1_delay,
    :rx2_delay,
    :gw_power,
    :max_eirp,
    :max_power,
    :min_power,
    :max_datr,
    :dcycle_init,
    :rxwin_init,
    :init_chans,
    :cflist
  ]

  def create(%__MODULE__{} = network) do
    LorawanServer.create("/networks", network)
  end
end
