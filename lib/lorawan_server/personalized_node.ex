defmodule LorawanBridge.LorawanServer.PersonalizedNode do
  alias LorawanBridge.LorawanServer

  @moduledoc """
  {"devaddr":"26011C22","profile":"GPS Node","appargs":null,"location":"org.entropealabs.clubhouse","nwkskey":"9AEAD09306E32B73DD547B8BFFDC20F9","appskey":"B6075BB5E4CE40A2A3EE7BDFDC230E2B","desc":null,"fcntup":null,"fcntdown":0}
  """
  defstruct [
    :devaddr,
    :profile,
    :appargs,
    :location,
    :nwkskey,
    :appskey,
    :desc,
    :fcntup,
    :fcntdown
  ]

  def create(%__MODULE__{} = node) do
    LorawanServer.create("/nodes", node)
  end
end
