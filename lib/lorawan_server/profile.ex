defmodule LorawanBridge.LorawanServer.Profile do
  alias LorawanBridge.LorawanServer

  @moduledoc """
  {"name":"GPS Node","group":"org.entropealabs.clubhouse","app":"GPS Parser","appid":null,"join":1,"fcnt_check":0,"txwin":0,"adr_mode":0,"adr_set":{"power":null,"datr":null,"chans":null},"max_datr":null,"dcycle_set":null,"rxwin_set":{"rx1_dr_offset":null,"rx2_dr":null,"rx2_freq":null},"request_devstat":true}
  """
  defstruct [
    :name,
    :group,
    :app,
    :appid,
    :join,
    :fcnt_check,
    :txwin,
    :adr_mode,
    :adr_set,
    :max_datr,
    :dcycle_set,
    :rxwin_set,
    :request_devstat
  ]

  def create(%__MODULE__{} = profile) do
    LorawanServer.create("/profiles", profile)
  end
end
