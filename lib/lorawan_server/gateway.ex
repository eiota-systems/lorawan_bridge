defmodule LorawanBridge.LorawanServer.Gateway do
  alias LorawanBridge.LorawanServer

  @moduledoc """
  {"mac":"a840411d09504150","area":"Clubhouse","tx_rfch":0,"ant_gain":10,"desc":null,"gpspos":{"lat":48.88,"lon":14.12},"gpsalt":42,"health_alerts":null,"ip_address":{"ip":null},"last_alive":null,"last_gps":null,"last_report":null}
  """
  defstruct [
    :mac,
    :area,
    :tx_rfch,
    :ant_gain,
    :desc,
    :gpspos,
    :gpsalt,
    :health_alerts,
    :ip_address,
    :last_alive,
    :last_gps,
    :last_report
  ]

  def create(%__MODULE__{} = gateway) do
    LorawanServer.create("/gateways", gateway)
  end
end
