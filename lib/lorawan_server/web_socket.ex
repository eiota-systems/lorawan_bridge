defmodule LorawanBridge.LorawanServer.WebSocket do
  use WebSockex

  alias LorawanBridge.LorawanServer
  alias Wampex.Client
  alias Wampex.Roles.Caller.Call
  alias Wampex.Roles.Dealer.Result
  alias Wampex.Roles.Publisher.Publish
  alias WebSockex.Conn

  require Logger

  @ping_interval 20_000
  @max_reconnect_interval 2 * 60_000

  def send_request(t, data) do
    WebSockex.cast(t, {:send_request, data})
  end

  def get_url(realm, profile) do
    "#{LorawanServer.get_ws_url()}/#{realm}/uplink/#{profile}"
  end

  def start_link(
        client: client,
        ids: ids,
        profile: profile,
        realm: realm
      ) do
    conn = Conn.new(get_url(realm, profile))

    WebSockex.start_link(
      conn,
      __MODULE__,
      %{
        client: client,
        profile: profile,
        realm: realm,
        ids: ids,
        connected: false,
        reconnect: true,
        reconnect_attempts: 1,
        devices: %{}
      },
      handle_initial_conn_failure: true
    )
  end

  defp get_backoff_delay(attempts) do
    backoff = :math.pow(2, attempts) * 100
    ceil(min(backoff, @max_reconnect_interval))
  end

  @impl true
  def handle_disconnect(_status, %{reconnect: false} = state) do
    {:ok, state}
  end

  @impl true
  def handle_disconnect(_status, %{reconnect_attempts: attempts} = state) do
    delay = get_backoff_delay(attempts)
    Logger.warn("Connection disconnected. Attempting reconnect in #{delay}")
    :timer.sleep(delay)
    {:reconnect, %{state | reconnect_attempts: attempts + 1}}
  end

  @impl true
  def handle_connect(_conn, state) do
    Process.send_after(self(), :ping, @ping_interval)
    {:ok, %{state | reconnect_attempts: 1, connected: true}}
  end

  @impl true
  def handle_ping(_ping, state) do
    {:reply, :pong, state}
  end

  @impl true
  def handle_pong(_pong, state) do
    {:ok, state}
  end

  @impl true
  def handle_cast({:send_request, data}, state) do
    Logger.debug("#{__MODULE__} Sending Request #{inspect(data)}")
    {:reply, {:text, Jason.encode!(data)}, state}
  end

  @impl true
  def handle_info(:ping, state) do
    Process.send_after(self(), :ping, @ping_interval)
    {:reply, :ping, state}
  end

  @impl true
  def handle_frame(
        {_type, message},
        %{profile: profile, client: client, devices: devices} = state
      ) do
    message = parse_frame(message)
    id = get_id(message)

    devices =
      case get_in(devices, [id]) do
        nil ->
          Logger.info("Connecting Device: #{inspect(message)}")
          connect_device(client, profile, id)
          Map.put(devices, id, true)

        _ ->
          devices
      end

    publish_frame(message, profile, client)
    {:ok, %{state | devices: devices}}
  end

  def parse_frame(message), do: Jason.decode!(message)

  def publish_frame(message, profile, client) do
    Logger.debug("Got Frame: #{inspect(message)}")

    case get_id(message) do
      nil ->
        :noop

      id ->
        Logger.debug("Publishing topic: #{profile}.#{id}")

        Client.publish(client, %Publish{
          topic: "#{profile}.#{id}",
          arg_kw: %{id: id, data: message}
        })
    end
  end

  def connect_device(client, profile, id) do
    device_profile = get_device_profile(profile)
    connect(client, id, device_profile)
  end

  def get_device_profile(profile) do
    :lorawan_bridge
    |> :code.priv_dir()
    |> Path.join("apps/#{profile}/device_profile.json")
    |> File.read!()
    |> Jason.decode!()
  end

  def connect(client, id, profile) do
    case Client.call(
           client,
           %Call{
             procedure: "device.connect",
             arg_kw: %{id: id, profile: profile}
           }
         ) do
      %Result{} = res ->
        res

      er ->
        Logger.warn("Can't connect to device shadow: #{inspect(er)}")
        :timer.sleep(1000)
        connect(client, id, profile)
    end
  end

  def get_id(%{"deveui" => id}), do: id
  def get_id(%{"devaddr" => id}), do: id
  def get_id(_), do: nil
end
