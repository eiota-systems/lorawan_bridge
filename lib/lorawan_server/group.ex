defmodule LorawanBridge.LorawanServer.Group do
  alias LorawanBridge.LorawanServer

  @moduledoc """
  {"name":"org.entropealabs.clubhouse","network":"EIOTA","subid":null,"admins":["admin"],"slack_channel":null,"can_join":true}
  """
  defstruct [:name, :network, :subid, :admins, :slack_channel, :can_join]

  def create(%__MODULE__{} = group) do
    LorawanServer.create("/groups", group)
  end
end
