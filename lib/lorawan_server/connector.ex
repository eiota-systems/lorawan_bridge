defmodule LorawanBridge.LorawanServer.Connector do
  alias LorawanBridge.LorawanServer

  @moduledoc """
  {"connid":"Test","app":"EIOTA AgTech","format":"json","uri":"ws:","publish_qos":1,"publish_uplinks":"/ws/uplink/{location}","publish_events":"/ws/events/{location}","subscribe_qos":null,"subscribe":null,"received":null,"enabled":true,"failed":null,"client_id":null,"auth":null,"name":null,"pass":null,"certfile":null,"keyfile":null,"health_alerts":null}
  """
  defstruct [
    :connid,
    :app,
    :format,
    :uri,
    :publish_qos,
    :publish_uplinks,
    :publish_events,
    :subscribe_qos,
    :subscribe,
    :received,
    :enabled,
    :failed,
    :client_id,
    :auth,
    :name,
    :pass,
    :certfile,
    :keyfile,
    :health_alerts
  ]

  def create(%__MODULE__{} = connector) do
    LorawanServer.create("/connectors", connector)
  end
end
