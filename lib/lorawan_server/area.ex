defmodule LorawanBridge.LorawanServer.Area do
  alias LorawanBridge.LorawanServer

  @moduledoc """
  {"name":"Clubhouse","region":"US902","admins":["admin"],"slack_channel":null,"log_ignored":false}
  """
  defstruct [:name, :region, :admins, :slack_channel, :log_ignored]

  def create(%__MODULE__{} = area), do: LorawanServer.create("/areas", area)
end
