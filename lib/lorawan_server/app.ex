defmodule LorawanBridge.LorawanServer.App do
  alias LorawanBridge.LorawanServer

  defstruct [
    :app,
    :uplink_fields,
    :payload,
    :parse_uplink,
    :event_fields,
    :parse_event,
    :build,
    :downlink_expires,
    :connectors
  ]

  def create(%__MODULE__{} = app) do
    LorawanServer.create("/handlers", app)
  end
end
