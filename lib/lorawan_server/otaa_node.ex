defmodule LorawanBridge.LorawanServer.OTAANode do
  alias LorawanBridge.LorawanServer
  require Logger

  @moduledoc """
  {"deveui":"0123123423498733","profile":"GPS Node","appargs":null,"appeui":"0123123423498733","appkey":"01231234234987330000000000000000","desc":null,"last_joins":null,"node":null}
  """
  defstruct [:deveui, :profile, :appargs, :appeui, :appkey, :desc, :node]

  def create(%__MODULE__{deveui: deveui} = device) do
    p = "/devices/#{deveui}"

    case LorawanServer.get(p) do
      %{"node" => node} ->
        LorawanServer.update(p, %__MODULE__{device | node: node})

      _ ->
        LorawanServer.create("/devices", device)
    end
  end
end
