defmodule LorawanBridge.RealmSync do
  use GenServer
  require Logger

  alias LorawanBridge.RealmSupervisor
  alias Wampex.Client
  alias Wampex.Roles.Broker.Event
  alias Wampex.Roles.Caller.Call
  alias Wampex.Roles.Dealer.Result
  alias Wampex.Roles.Subscriber.Subscribe

  def start_link(client: client, admin_realm: admin_realm) do
    GenServer.start_link(__MODULE__, {client, admin_realm}, name: __MODULE__)
  end

  def init({client, admin_realm}) do
    {:ok, %{}, {:continue, {client, admin_realm}}}
  end

  def handle_continue({client, admin_realm}, state) do
    Client.add(client, self())
    subscribe(client)
    start_realms(client, admin_realm)
    {:noreply, state}
  end

  def start_realms(client, admin_realm) do
    tenants = get_tenants(client)
    realms = Enum.flat_map(tenants, fn t -> get_in(t, ["realms"]) end)
    Logger.info("Starting Realms: #{inspect(realms)}")

    Enum.each(realms, fn
      %{"uri" => ^admin_realm} ->
        :noop

      %{"uri" => realm, "tenant" => tenant} ->
        start_realm(realm, tenant)
    end)
  end

  def get_tenants(client) do
    case Client.call(client, %Call{procedure: "admin.get_tenants"}) do
      %Result{arg_list: tenants} ->
        tenants

      er ->
        Logger.error("Can't load tenants: #{inspect(er)}")
        :timer.sleep(300)
        get_tenants(client)
    end
  end

  def handle_info({:connected, client}, state) do
    subscribe(client)
    {:noreply, state}
  end

  def handle_info(%Event{arg_kw: %{"uri" => realm, "tenant" => tenant}}, state) do
    Logger.info("RealmSync Got New Realm Event: #{inspect(realm)}")
    start_realm(realm, tenant)
    {:noreply, state}
  end

  defp start_realm(realm, tenant) do
    RealmSupervisor.start_realm(realm, tenant)
  end

  defp subscribe(client) do
    Client.subscribe(client, %Subscribe{topic: "admin.realm.created"})
  end
end
