defmodule LorawanBridge.LorawanServer do
  require Logger

  def create(path, obj) do
    "#{get_api_url()}#{path}"
    |> Mojito.post(headers(), Jason.encode!(Map.from_struct(obj)))
    |> handle_create()
  end

  def update(path, obj) do
    "#{get_api_url()}#{path}"
    |> Mojito.put(headers(), Jason.encode!(Map.from_struct(obj)))
    |> handle_create()
  end

  def get(path) do
    "#{get_api_url()}#{path}"
    |> Mojito.get(headers())
    |> handle_create()
  end

  def handle_create({:ok, %Mojito.Response{status_code: 200, body: body}}),
    do: Jason.decode!(body)

  def handle_create(er), do: Logger.error(inspect(er))

  def headers do
    [
      {"content-type", "application/json; charset=utf-8"},
      Mojito.Headers.auth_header("admin", "admin")
    ]
  end

  def get_api_url do
    System.get_env("LORAWAN_SERVER_API") || "http://localhost:8083/api"
  end

  def get_ws_url do
    System.get_env("LORAWAN_SERVER_WS") || "ws://localhost:8083/ws"
  end
end
