defmodule LorawanBridge.RealmWorker do
  use Supervisor
  require Logger

  alias LorawanBridge.LorawanServer.WebSocket

  def start_link(realm: realm, config: conf, client: client) do
    Supervisor.start_link(__MODULE__, {realm, conf, client})
  end

  def init({realm, conf, client}) do
    streams = get_streams(conf)
    ids = get_ids(conf)

    children =
      streams
      |> Enum.with_index()
      |> Enum.map(fn {profile, id} ->
        Supervisor.child_spec(
          %{
            id: id,
            start:
              {WebSocket, :start_link,
               [[client: client, ids: ids, profile: profile, realm: realm]]}
          },
          []
        )
      end)

    Logger.info(inspect(children))
    opts = [strategy: :one_for_one]
    Supervisor.init(children, opts)
  end

  def get_ids(%{"areas" => areas}) do
    areas
    |> Enum.flat_map(fn %{"nodes" => nodes} -> Enum.map(nodes, &get_id(&1)) end)
    |> Enum.uniq()
  end

  def get_streams(%{"areas" => areas}) do
    areas
    |> Enum.flat_map(fn %{"nodes" => nodes} -> Enum.map(nodes, &get_in(&1, ["profile"])) end)
    |> Enum.uniq()
  end

  def get_id(%{"devaddr" => id}), do: id
  def get_id(%{"deveui" => id}), do: id
  def get_id(_), do: nil
end
