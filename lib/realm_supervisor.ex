defmodule LorawanBridge.RealmSupervisor do
  use DynamicSupervisor
  alias LorawanBridge.RealmClient
  require Logger

  def start_link(platform_password: password, url: url) do
    DynamicSupervisor.start_link(__MODULE__, {password, url}, name: __MODULE__)
  end

  @impl true
  def init({password, url}) do
    opts = [strategy: :one_for_one, extra_arguments: [password, url]]
    DynamicSupervisor.init(opts)
  end

  def start_realm(realm, tenant) do
    spec = %{id: realm, start: {RealmClient, :start_link, [realm, tenant]}}
    resp = DynamicSupervisor.start_child(__MODULE__, spec)
    Logger.info("Response: #{inspect(resp)}")
  end
end
