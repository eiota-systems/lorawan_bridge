defmodule LorawanBridge.RealmClient do
  use Supervisor

  require Logger

  alias LorawanBridge.LorawanServer.{
    Area,
    Gateway,
    OTAANode,
    PersonalizedNode
  }

  alias LorawanBridge.RealmWorker
  alias Wampex.Client
  alias Wampex.Client.Session
  alias Wampex.Client.{Authentication, Realm}
  alias Wampex.Roles.{Callee, Caller, Subscriber}

  def start_link(password, url, realm, tenant) do
    Supervisor.start_link(__MODULE__, {password, url, realm, tenant}, name: sup_name(realm))
  end

  @impl true
  def init({password, url, realm, tenant}) do
    case get_realm_config(realm) do
      :not_found -> :ignore
      conf -> handle_startup(password, url, realm, tenant, conf)
    end
  end

  def handle_startup(password, url, realm, _tenant, conf) do
    authentication = %Authentication{
      authid: "platform@" <> realm,
      authmethods: ["wampcra"],
      secret: password
    }

    r = %Realm{name: realm, authentication: authentication}

    roles = [Callee, Caller, Subscriber]
    session = %Session{url: url, realm: r, roles: roles}

    realm_client = client_name(realm)

    seed_lorawan_server(realm, conf)

    children = [
      {Client, name: realm_client, session: session, reconnect: false},
      {RealmWorker, realm: realm, config: conf, client: realm_client}
    ]

    opts = [strategy: :rest_for_one]
    Supervisor.init(children, opts)
  end

  defp seed_lorawan_server(realm, %{"areas" => areas}) do
    Enum.flat_map(areas, fn %{"gateways" => gws, "nodes" => nodes} = ar ->
      %{"id" => area_id} = create_area(ar)
      create_area_gateways(area_id, gws)
      create_area_nodes(realm, nodes)
    end)
  end

  defp create_area_gateways(area_id, gateways) do
    Enum.each(gateways, fn %{"mac" => mac, "gps" => %{"lat" => lat, "lon" => lon, "alt" => alt}} ->
      Gateway.create(%Gateway{
        mac: mac,
        area: area_id,
        tx_rfch: 0,
        ant_gain: 10,
        gpspos: %{lat: lat, lon: lon},
        gpsalt: alt
      })
    end)
  end

  defp create_area_nodes(realm, nodes) do
    Enum.map(nodes, fn
      %{
        "type" => "personalized",
        "profile" => profile,
        "devaddr" => addr,
        "nwkskey" => nwkskey,
        "appskey" => appskey,
        "deveui" => deveui
      } ->
        PersonalizedNode.create(%PersonalizedNode{
          devaddr: addr,
          profile: profile,
          nwkskey: nwkskey,
          appskey: appskey,
          fcntdown: 0
        })

        OTAANode.create(%OTAANode{
          deveui: deveui,
          profile: profile,
          appeui: deveui,
          appkey: appskey,
          appargs: realm,
          node: addr
        })

      %{
        "type" => "otaa",
        "profile" => profile,
        "deveui" => addr,
        "appeui" => appeui,
        "appkey" => appkey
      } ->
        OTAANode.create(%OTAANode{
          deveui: addr,
          profile: profile,
          appeui: appeui,
          appkey: appkey,
          appargs: realm
        })
    end)
  end

  defp create_area(%{"name" => name, "region" => region, "admins" => admins}) do
    Area.create(%Area{name: name, region: region, admins: admins, log_ignored: false})
  end

  defp get_realm_config(realm) do
    :lorawan_bridge
    |> :code.priv_dir()
    |> Path.join("/realms/#{realm}.json")
    |> File.read!()
    |> Jason.decode!()
  rescue
    _er -> :not_found
  end

  defp sup_name(realm), do: Module.concat([__MODULE__, realm, Sup])
  defp client_name(realm), do: Module.concat([__MODULE__, realm, Cli])
end
