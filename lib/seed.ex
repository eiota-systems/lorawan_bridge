defmodule LorawanBridge.Seed do
  alias LorawanBridge.LorawanServer.{App, Connector, Group, Profile, Network}

  def create do
    %{"id" => network_id} = create_network()
    %{"id" => group_id} = create_group(network_id)
    create_apps(group_id)
  end

  def create_apps(group_id) do
    path = get_apps_path()
    apps = get_apps(path)

    Enum.each(apps, fn app ->
      profile = get_app_profile(app)
      parsers = get_app_parsers(app)
      %{"id" => app_id} = create_app(profile, parsers)
      create_profile(profile, group_id, app_id)
      create_connector(profile, app_id)
    end)
  end

  def create_profile(%{"name" => name}, group_id, app_id) do
    Profile.create(%Profile{
      name: name,
      group: group_id,
      app: app_id,
      appid: app_id,
      join: 1,
      fcnt_check: 2,
      txwin: 0,
      adr_mode: 0,
      adr_set: %{power: nil, datr: nil, chans: nil},
      rxwin_set: %{rx1_dr_offset: nil, rx2_dr: nil, rx2_freq: nil},
      request_devstat: true
    })
  end

  def create_app(
        %{
          "name" => name,
          "fields" => fields,
          "payload_type" => pt,
          "downlink_expires" => expires
        },
        [uplink]
      ) do
    App.create(%App{
      app: name,
      uplink_fields: fields,
      payload: pt,
      parse_uplink: uplink,
      downlink_expires: expires
    })
  end

  def get_app_parsers(app) do
    parsers = ["uplink_parser.erl"]

    Enum.map(parsers, fn p ->
      app
      |> Path.join(p)
      |> File.read!()
    end)
  end

  def get_app_profile(path) do
    path
    |> Path.join("profile.json")
    |> File.read!()
    |> Jason.decode!()
  end

  def get_apps_path do
    :lorawan_bridge
    |> :code.priv_dir()
    |> Path.join("apps")
  end

  def get_apps(path) do
    Enum.map(File.ls!(path), fn d -> Path.join(path, d) end)
  end

  def create_connector(%{"name" => name}, app_id) do
    Connector.create(%Connector{
      connid: "#{name}",
      app: app_id,
      format: "json",
      uri: "ws:",
      publish_qos: 1,
      publish_uplinks: "/ws/{appargs}/uplink/#{name}",
      publish_events: "/ws/{appargs}/events/#{name}",
      enabled: true
    })
  end

  def create_group(network_id) do
    Group.create(%Group{
      name: "EIOTA",
      network: network_id,
      admins: ["admin"],
      can_join: true
    })
  end

  def create_network do
    Network.create(%Network{
      name: "EIOTA",
      netid: "000000",
      region: "US902",
      tx_codr: "4/5",
      join1_delay: 5,
      join2_delay: 6,
      rx1_delay: 1,
      rx2_delay: 2,
      gw_power: 26,
      max_eirp: 30,
      max_power: 0,
      min_power: 10,
      max_datr: 4,
      dcycle_init: 0,
      rxwin_init: %{rx1_dr_offset: 0, rx2_dr: 8, rx2_freq: 923.3},
      init_chans: "0-71"
    })
  end
end
