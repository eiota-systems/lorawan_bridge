defmodule LorawanBridge do
  @moduledoc """
  Documentation for AlertManager.
  """
  use Application

  alias LorawanBridge.{RealmSupervisor, RealmSync, Seed}
  alias Wampex.Client
  alias Wampex.Client.Session
  alias Wampex.Client.{Authentication, Realm}
  alias Wampex.Roles.{Caller, Subscriber}
  require Logger

  def start(_, _) do
    # topologies = Application.get_env(:lorawan_bridge, :topologies)
    url = System.get_env("WAMP_SERVER") || "ws://localhost:4000/ws"
    platform_password = System.get_env("PLATFORM_PASSWORD")
    admin_realm = System.get_env("ADMIN_REALM")

    authentication = %Authentication{
      authid: System.get_env("ADMIN_AUTHID"),
      authmethods: ["wampcra"],
      secret: System.get_env("ADMIN_PASSWORD")
    }

    realm = %Realm{name: admin_realm, authentication: authentication}

    admin_roles = [Caller, Subscriber]
    admin_session = %Session{url: url, realm: realm, roles: admin_roles}

    children = [
      Task.child_spec(fn -> Seed.create() end),
      {RealmSupervisor, platform_password: platform_password, url: url},
      {Client, name: AdminClient, session: admin_session, reconnect: true},
      {RealmSync, client: AdminClient, admin_realm: admin_realm}
    ]

    Supervisor.start_link(children, strategy: :one_for_one)
  end
end
