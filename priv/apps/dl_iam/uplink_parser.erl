fun(Fields, <<2, DeviceId:16, Flags:16/bitstring, Bytes/binary>> = Binary) 
	when is_binary(Binary) ->
	Sensors = [
    #{
      length => 1,
      values => [
        #{
          name => <<"battery_voltage">>,
          convert => fun(X) -> lists:nth(0 + 1, X) / 1000 end,
          unit => <<"V"/utf8>>
        }
      ]
    },
    #{
      length => 2,
      values => [
        #{
          name => <<"temperature">>,
          convert => fun(X) -> 175 * lists:nth(0 + 1, X) / 65535 - 45 end,
          unit => <<"°C"/utf8>>
        },
        #{
          name => <<"humidity">>,
          convert => fun(X) -> 100 * lists:nth(1 + 1, X) / 65535 end,
          unit => <<"%"/utf8>>
        }
      ]
    },
    #{
      length => 1,
      values => [
        #{
          name => <<"barometric_pressure">>,
          convert => fun(X) -> lists:nth(0 + 1, X) * 2 end,
          unit => <<"Pa"/utf8>>
        }
      ]
    },
    #{
      length => 2,
      values => [
        #{
          name => <<"ambient_light">>,
          convert => fun(X) -> lists:nth(0 + 1, X) end
        },
        #{
          name => <<"ambient_light_infrared">>,
          convert => fun(X) -> lists:nth(1 + 1, X) end
        },
        #{
          name => <<"illuminance">>,
          convert => fun(X) -> max(max(1.0 * lists:nth(0 + 1, X) - 1.64 * lists:nth(1 + 1, X), 0.59 * lists:nth(0 + 1, X) - 0.86 * lists:nth(1 + 1, X)), 0) * 1.5504 end,
          unit => <<"lx"/utf8>>
        }
      ]
    },
    #{
      length => 3,
      values => [
        #{
          name => <<"co2">>,
          convert => fun(X) -> lists:nth(0 + 1, X) - 32768 end,
          unit => <<"ppm"/utf8>>
        },
        #{
          name => <<"co2_status">>,
          convert => fun(X) -> lists:nth(1 + 1, X) end
        },
        #{
          name => <<"raw_infrared">>,
          convert => fun(X) -> lists:nth(2 + 1, X) end
        }
      ]
    },
    #{
      length => 1,
      values => [
        #{
          name => <<"activity_counter">>,
          convert => fun(X) -> lists:nth(0 + 1, X) end
        }
      ]
    },
    #{
      length => 1,
      values => [
        #{
          name => <<"voc">>,
          convert => fun(X) -> lists:nth(0 + 1, X) end,
          unit => <<"ppb"/utf8>>
        }
      ]
    }
  ],
	
	ValueFun = fun 
		Value([], _X) -> #{};
		Value([#{convert := Convert, name := Name} | RestDefs], X) ->
			Values = Value(RestDefs, X),
      Val = Convert(X),
      maps:put(Name, Val, Values);
		Value([_NoConvert | RestDefs], X) -> Value(RestDefs, X)
	end,

	SensorFun = fun 
		Sensor(_Words, _Flags, []) -> #{};
		Sensor(Words, <<Flags:15, 0:1>>, [_Cur | Rest]) ->
			Sensor(Words, <<0:1, Flags:15>>, Rest);
  	Sensor(Words, <<Flags:15, 1:1>>, [#{length := Len, values := ValueDefs} | RestDefs]) ->
    	{X, RestWords} = lists:split(Len, Words),
    	Values = ValueFun(ValueDefs, X),
    	RestSensors = Sensor(RestWords, <<0:1, Flags:15>>, RestDefs),
    	maps:merge(Values, RestSensors)
	end,

	

	BytesToWordsFun = fun
		BytesToWords(<<>>, Words) -> lists:reverse(Words);
		BytesToWords(<<Word:16, Rest/binary>>, Words) ->
	    BytesToWords(Rest, [Word | Words])
	end,


	Words = BytesToWordsFun(Bytes, []),
  maps:merge(
		Fields#{<<"protocol_version">> => 2, <<"device_id">> => DeviceId}, 
		SensorFun(Words, Flags, Sensors)
	)
end.
