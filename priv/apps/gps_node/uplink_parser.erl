fun(Fields, <<Data/binary>>) ->
  [Lat, Lon, Alt] = string:tokens(binary_to_list(Data), ","),
  Lat1 = string:chomp(Lat),
  Lon1 = string:chomp(Lon),
  Alt1 = string:chomp(Alt),
  {Flat, _} = string:to_float(Lat1),
  {Flon, _} = string:to_float(Lon1),
  {Falt, _} = string:to_float(Alt1),
  Fields#{lat => Flat, lon => Flon, alt => Falt, gps => list_to_binary(lists:concat(lists:join(",", [Lat1, Lon1, Alt1])))}
end.
