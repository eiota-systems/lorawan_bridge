fun(Fields, <<Data/binary>>) ->
  [Lat, Lon, Alt] = string:tokens(binary_to_list(Data), ","),
  {Lat1, _} = string:to_float(string:chomp(Lat)),
  {Lon1, _} = string:to_float(string:chomp(Lon)),
  {Alt1, _} = string:to_float(string:chomp(Alt)),
  Fields#{lat => Lat1, lon => Lon1, alt => Alt1}
end.
