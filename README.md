# LORAWanBridge

The LORAWan Bridge interfaces with the [LORAWan Server](https://github.com/gotthardp/lorawan-server) to allow lorawan devices to publish data into the EIOTA Platform.

The EIOTA platform utlizes a forked version of the server available at [https://gitlab.com/eiota-systems/lorawan-server](https://gitlab.com/eiota-systems/lorawan-server)

This version just updates the docker builds and uses a dark theme for the administratie interface.

It contains all configuration for the LORAWan Server as well as the device profiles needed to connect to the device shadow system.

The LORAWan server provides a method of publishing incoming lorawan packets over websockets, the bridge utilizes this along with the REST administrative interface to configure and receive all incoming packets, and forward them on to the EIOTA Router.

The LORAWan configuration is handled declaratively through configuration files.

An `area` in the LORAWan Server is roughly equivalent to a Realm within the WAMP protocol.

We configure an area/realm using a JSON file in the form of

```json
{
  "areas": [
    {
      "name": "Clubhouse",
      "region": "US902",
      "admins": ["admin"],
      "gateways": [
        {"mac": "dca632fffe36873c", "gps": {"lat": 42.647504, "lon": -86.104574, "alt": 42}}
      ],
      "nodes": [
        {"type": "personalized", "profile": "gps_node", "deveui": "68AE784DDC9B0404", "devaddr": "26011C22", "nwkskey": "9AEAD09306E32B73DD547B8BFFDC20F9", "appskey": "B6075BB5E4CE40A2A3EE7BDFDC230E2B"},
        {"type": "otaa", "profile": "dl_iam", "deveui": "70B3D57BA0000EEA", "appeui": "70B3D57ED00006B2", "appkey": "536AD262225AAFF625FCA3ABFC41B200"}
      ]
    }
  ]
}
```

The filename is important here, as it defines the realm -> area translation. The filename for the example above is `org.entropealabs.clubhouse.json`

All realm configuration files live in [priv/realms](priv/realms)

We also have to consider the gatesways and devices that are available for a given area, in the above configuration you can see we support and array of both.

Within the list of nodes, each node contains a field `profile`. This helps us map a LORAWan device to a device profile within the platform.

All device profile files live in [priv/apps](priv/apps), where the `profile` key above directly relates to the directory name within `priv/apps`

This file lives at [priv/apps/dl_iam/device_profile.json](priv/apps/dl_iam/device_profile.json)

The term `app` is what is used within the LORAWan Server.

Let's look at the profile for the [dl_iam](https://www.decentlab.com/products/indoor-ambiance-monitor-including-co2-tvoc-and-motion-sensor-for-lorawan) device. IAM in this case is an acronym for Indoor Ambient Monitor.

```json
{
  "version": "1.0.0",
  "namespace": "com.decent_lab",
  "displayName": "DL-IAM",
  "description": "Indoor Ambient Monitor",
  "categories": ["co2", "temperature", "pressure", "humidity", "ambient light", "voc", "occupancy"],
  "system": {
    "manufacturer": "Decent Lab",
    "type": "dl_iam",
    "model": "prod"
  },
  "topics": [
    {
      "topic": "{{system.type}}.{{id}}",
      "payload": {}
    }
  ],
  "procedures": [
  ],
  "metric_parsers": {
    "{{system.type}}.{{id}}": [
      {"key": "battery_voltage", "path": "$.battery_voltage"},
      {"key": "temperature", "path": "$.temperature"},
      {"key": "humidity", "path": "$.humidity"},
      {"key": "barometric_pressure", "path": "$.barometric_pressure"},
      {"key": "ambient_light", "path": "$.ambient_light_infrared"},
      {"key": "illuminance", "path": "$.illuminance"},
      {"key": "co2", "path": "$.co2"},
      {"key": "co2_status", "path": "$.co2_status"},
      {"key": "raw_infrared", "path": "$.raw_infrared"},
      {"key": "activity_counter", "path": "$.activity_counter"},
      {"key": "voc", "path": "$.voc"}
    ]
  },
  "event_parsers": {
    "{{system.type}}.{{id}}": [
      {"key": "voc", "path": "$.voc"}
    ]
  }
}
```

And here's the [profile.json](priv/apps/dl_iam/profile.json) file

```json
{
  "name": "dl_iam",
  "type": "otaa",
  "admins": ["admin"],
  "fields": ["appid", "devaddr", "deveui", "appargs", "battery", "data", "datetime", "rssi", "fcnt", "freq", "datr", "mac"],
  "payload_type": "ascii",
  "downlink_expires": "never"
}
```

This is pretty standard across all of the devices, although you will change the name, and perhaps type depending on how the device is activated on the LOARWan network.

And finally the parser at [priv/apps/dl_iam/uplink_parser.erl](priv/apps/dl_iam/uplink_parser.erl).

```erlang
fun(Fields, <<2, DeviceId:16, Flags:16/bitstring, Bytes/binary>> = Binary) 
	when is_binary(Binary) ->
	Sensors = [
    #{
      length => 1,
      values => [
        #{
          name => <<"battery_voltage">>,
          convert => fun(X) -> lists:nth(0 + 1, X) / 1000 end,
          unit => <<"V"/utf8>>
        }
      ]
    },
    #{
      length => 2,
      values => [
        #{
          name => <<"temperature">>,
          convert => fun(X) -> 175 * lists:nth(0 + 1, X) / 65535 - 45 end,
          unit => <<"°C"/utf8>>
        },
        #{
          name => <<"humidity">>,
          convert => fun(X) -> 100 * lists:nth(1 + 1, X) / 65535 end,
          unit => <<"%"/utf8>>
        }
      ]
    },
    #{
      length => 1,
      values => [
        #{
          name => <<"barometric_pressure">>,
          convert => fun(X) -> lists:nth(0 + 1, X) * 2 end,
          unit => <<"Pa"/utf8>>
        }
      ]
    },
    #{
      length => 2,
      values => [
        #{
          name => <<"ambient_light">>,
          convert => fun(X) -> lists:nth(0 + 1, X) end
        },
        #{
          name => <<"ambient_light_infrared">>,
          convert => fun(X) -> lists:nth(1 + 1, X) end
        },
        #{
          name => <<"illuminance">>,
          convert => fun(X) -> max(max(1.0 * lists:nth(0 + 1, X) - 1.64 * lists:nth(1 + 1, X), 0.59 * lists:nth(0 + 1, X) - 0.86 * lists:nth(1 + 1, X)), 0) * 1.5504 end,
          unit => <<"lx"/utf8>>
        }
      ]
    },
    #{
      length => 3,
      values => [
        #{
          name => <<"co2">>,
          convert => fun(X) -> lists:nth(0 + 1, X) - 32768 end,
          unit => <<"ppm"/utf8>>
        },
        #{
          name => <<"co2_status">>,
          convert => fun(X) -> lists:nth(1 + 1, X) end
        },
        #{
          name => <<"raw_infrared">>,
          convert => fun(X) -> lists:nth(2 + 1, X) end
        }
      ]
    },
    #{
      length => 1,
      values => [
        #{
          name => <<"activity_counter">>,
          convert => fun(X) -> lists:nth(0 + 1, X) end
        }
      ]
    },
    #{
      length => 1,
      values => [
        #{
          name => <<"voc">>,
          convert => fun(X) -> lists:nth(0 + 1, X) end,
          unit => <<"ppb"/utf8>>
        }
      ]
    }
  ],
	
	ValueFun = fun 
		Value([], _X) -> #{};
		Value([#{convert := Convert, name := Name} | RestDefs], X) ->
			Values = Value(RestDefs, X),
      Val = Convert(X),
      maps:put(Name, Val, Values);
		Value([_NoConvert | RestDefs], X) -> Value(RestDefs, X)
	end,

	SensorFun = fun 
		Sensor(_Words, _Flags, []) -> #{};
		Sensor(Words, <<Flags:15, 0:1>>, [_Cur | Rest]) ->
			Sensor(Words, <<0:1, Flags:15>>, Rest);
  	Sensor(Words, <<Flags:15, 1:1>>, [#{length := Len, values := ValueDefs} | RestDefs]) ->
    	{X, RestWords} = lists:split(Len, Words),
    	Values = ValueFun(ValueDefs, X),
    	RestSensors = Sensor(RestWords, <<0:1, Flags:15>>, RestDefs),
    	maps:merge(Values, RestSensors)
	end,

	

	BytesToWordsFun = fun
		BytesToWords(<<>>, Words) -> lists:reverse(Words);
		BytesToWords(<<Word:16, Rest/binary>>, Words) ->
	    BytesToWords(Rest, [Word | Words])
	end,


	Words = BytesToWordsFun(Bytes, []),
  maps:merge(
		Fields#{<<"protocol_version">> => 2, <<"device_id">> => DeviceId}, 
		SensorFun(Words, Flags, Sensors)
	)
end.
```

When the bridge starts it will iterate over all of the relevant directories and files, and configure the LORAWan server, meaning that we need no persistance for the server itself, or the bridge.
