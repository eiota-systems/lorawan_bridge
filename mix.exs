defmodule LorawanBridge.MixProject do
  use Mix.Project

  def project do
    [
      app: :lorawan_bridge,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      releases: releases(),
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {LorawanBridge, []},
      extra_applications: [:logger]
    ]
  end

  defp releases do
    [
      app: [
        include_executables_for: [:unix],
        applications: [lorawan_bridge: :permanent, runtime_tools: :permanent],
        cookie: "lorawan_bridge_cookie"
      ]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:websockex, "~> 0.4.2"},
      {:mojito, "~> 0.6.3"},
      {:jason, "~> 1.2"},
      {:wampex_client,
       git: "https://gitlab.com/entropealabs/wampex_client.git",
       tag: "93704158ba2fb4db7bb19a454dad3367ecf5d03f"}
    ]
  end
end
